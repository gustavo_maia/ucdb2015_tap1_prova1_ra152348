package ucdb;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet({ "/alucontroller", "/AlunoServlet", "/AlunoController", "/AlunoController.do" })
public class AlunoController extends HttpServlet {
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String acao = req.getParameter("acao");
		if(acao.equals("Excluir")){
			int id = Integer.parseInt(req.getParameter("id"));
			GerenciadorAluno ga = new GerenciadorAluno();
			ga.excluir(id);
			resp.setContentType("text/html");
			resp.getWriter()
			.print("<script> window.alert('Excluido Sucesso!'); location.href='AlunoController?acao=Listar'</script>");
		}else if (acao.equals("Listar")){
		
			//Pegar a lista
			GerenciadorAluno ga = new GerenciadorAluno();
			List<Aluno> alunos = ga.getProdutos();
			
			//Adiciona a lista no request como atributo
			
			req.setAttribute("alu", alunos);
	
			//Levar para o JSP
			RequestDispatcher view = req.getRequestDispatcher("Alunos.jsp");
			view.forward(req, resp);
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String acao = request.getParameter("acao");
		if(acao.equals("Salvar")){
			String nome = request.getParameter("nome");
			String sexo =  request.getParameter("sexo");
			String ativo = request.getParameter("ativo");
			String cidade = request.getParameter("cidade");
			String estado = request.getParameter("estado");
			String profissao = request.getParameter("profissao");
			String materia = request.getParameter("materia");
			Double nota1 = Double.parseDouble(request.getParameter("nota1"));
			Double nota2 = Double.parseDouble(request.getParameter("nota2"));
			Double nota3 = Double.parseDouble(request.getParameter("nota3"));
			Double nota4 = Double.parseDouble(request.getParameter("nota4"));
			
			Double media = (nota1+nota2+nota3+nota4)/4;
			
			Aluno aluno = new Aluno();
			if(ativo==null){
				aluno.setAtivo("Nao");
			}else{
				aluno.setAtivo(ativo);
			}
			aluno.setCidade(cidade);
			aluno.setEstado(estado);
			aluno.setMateria(materia);
			aluno.setNome(nome);
			aluno.setProfissao(profissao);
			aluno.setSexo(sexo);
			aluno.setNota1(nota1);
			aluno.setNota2(nota2);
			aluno.setNota3(nota3);
			aluno.setNota4(nota4);
			aluno.setMedia(media);
			
			
			GerenciadorAluno ga = new GerenciadorAluno();
			ga.salvar(aluno);
			response.setContentType("text/html");
			response.getWriter()
			.print("<script> window.alert('Salvo Sucesso!'); location.href='AlunoController?acao=Listar'</script>");
		}else if(acao.equals("Listar")){
			doGet(request, response);
		}
		
	}
	
}
